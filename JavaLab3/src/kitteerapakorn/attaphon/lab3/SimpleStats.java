package kitteerapakorn.attaphon.lab3;

import java.util.Arrays;

public class SimpleStats 
{
	public static void main(String[] args)
	{
		int num = Integer.parseInt(args[0]);
		double sum = 0;
		double[] GPAs = new double[num];
		
		for(int i = 1 ; i <= num ; i++)
		{
			GPAs[i-1] = Double.parseDouble(args[i]);
		}
		
		System.out.println("For the input GPAs:");
		for(int i = 0 ; i < num ; i++)
		{
			System.out.print(GPAs[i] + " ");
			sum += GPAs[i];
		}
		
		Arrays.sort(GPAs);
		
		double Avg = sum / num;
		
		System.out.println("\nStats:");
		System.out.println("Avg GPA is " + Avg);
		System.out.println("Min GPA is " + GPAs[0]);
		System.out.println("Max GPA is " + GPAs[num-1]);
		
	}
}
