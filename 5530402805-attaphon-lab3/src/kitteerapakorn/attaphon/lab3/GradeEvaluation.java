package kitteerapakorn.attaphon.lab3;

public class GradeEvaluation
{
	public static void main(String[] args)
	{
		if(args.length != 1)
		{
			System.err.println("Usage:GradeEvaluation <score>");
			System.exit(1);
		}
		
		double score = Double.parseDouble(args[0]);
		
		computeGrade(score);
	}
	
	////////// compute///////////
	
	public static void computeGrade(double score)
	{
		char grade = 0;
		
		if(score > 100 || score < 0)
		{
			System.out.println(score + " is an invalid score. Please enter score in the range 0-100");
		} 
		else 
		{
			if(score >= 80)
			{
				grade = 'A';
			}
			else if(score >= 70)
			{
				grade = 'B';
			}
			else if(score >= 60)
			{
				grade = 'C';
			}
			else if(score >= 50)
			{
				grade = 'D';
			}
			else
			{
				grade = 'F';
			}
			
		}
		
		System.out.println(grade);
		
	}
}
