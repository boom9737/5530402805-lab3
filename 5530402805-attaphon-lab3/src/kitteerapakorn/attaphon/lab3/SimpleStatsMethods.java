package kitteerapakorn.attaphon.lab3;

import java.util.Arrays;

public class SimpleStatsMethods 
{
	static int numGPAs;
	static double[] nums;
	static double Avg;
	
	public static void acceptInputs(String[] args)
	{
		for(int i = 1 ; i <= numGPAs ; i++)
		{
			nums[i-1] = Double.parseDouble(args[i]);
		}
	}
	
	public static void displayStats(double[] nums)
	{
		double sum = 0;
		
		System.out.println("For the input GPAs:");
		for(int i = 0 ; i < numGPAs ; i++)
		{
			System.out.print(nums[i] + " ");
			sum += nums[i];
		}
		
		Arrays.sort(nums);
		
		Avg = sum / numGPAs;
		
		System.out.println("\nStats:");
		System.out.println("Avg GPA is " + Avg);
		System.out.println("Min GPA is " + nums[0]);
		System.out.println("Max GPA is " + nums[numGPAs-1]);	
	}
	
	public static void main(String[] args)
	{
		if(args.length < 2)
		{
			System.out.println("Usage:<SimpleStats> <numGPAs> <GPA>..");
			System.exit(0);
		}
		numGPAs = Integer.parseInt(args[0]);
		nums = new double[numGPAs];
		
		acceptInputs(args);
		displayStats(nums);
		
	}
}