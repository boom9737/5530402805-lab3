package kitteerapakorn.attaphon.lab3;

public class Permutation
{
	public static  void main(String[] args)
	{
		if(args.length != 2)
		{
			System.out.println("Usage:Permutation <n> <k>");
			System.exit(1);
		}
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		long result = permute(n, k);
		System.out.println(n + "!/(" + n + "-" + k + ")! = " + result);
	}
	
	public static int permute(int n, int k)
	{
		int ans1 = 1, ans2 = 1, ans3 = 1;
		for(int i = 1; i<=n; i++)
		{
			ans1 *= i;
			
			if(i<=n-k)
			{
				ans2 *= i;
			}
			
		}
		ans3 = ans1 / ans2;
		
		System.out.println(n + "! = " + ans1);
		System.out.println("(" + n + "-" + k + ") = " + ans2);
	return ans3;
	}

}
